<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\select2;

use Yii;

/**
 * Asset bundle for widget [[Select2]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class ThemeDefaultAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/select2/assets';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/default.css',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        'exoo\select2\Select2Asset',
        // 'exoo\exookit\ExookitAsset',
    ];
}
