<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\select2;

use Yii;

/**
 * Asset bundle for widget [[Select2]].
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Select2Asset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/select2';
    /**
     * @inheritdoc
     */
    public $js = [
        'dist/js/select2.min.js',
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'dist/css/select2.min.css',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
    	'exoo\uikit\UikitAsset',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $language = substr(Yii::$app->language, 0, 2);
        $this->js[] = 'dist/js/i18n/' . $language . '.js';
    }
}
