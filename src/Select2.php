<?php
/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2016
 * @package
 * @version 1.0.0
 */

namespace exoo\select2;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\widgets\InputWidget;

/**
 * Select2 is a jQuery based replacement for select boxes.
 *
 * For example to use the Select2 with a [[\yii\base\Model|model]]:
 *
 * ```php
 * echo Select2::widget([
 *     'model' => $model,
 *     'attribute' => 'text',
 *     'items' => 'items',
 * ]);
 * ```
 *
 * The following example will use the name property instead:
 *
 * ```php
 * echo Select2::widget([
 *     'name'  => 'text',
 *     'value'  => $value,
 *     'items' => 'items',
 * ]);
 * ```
 *
 * You can also use this widget in an [[\yii\widgets\ActiveForm|ActiveForm]] using the [[\yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'text')->widget(Select2::classname(), [
 *     'items' => 'items',
 *     'options' => ['multiple' => 'multiple'],
 *     'clientOptions' => ['placeholder' => 'Select'],
 * ]) ?>
 * ```
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Select2 extends InputWidget
{
    /**
     * @var array list of select items. Each array element can be either an array [1 => 'One'],
     * or an multidimensional array representing a single select with the following structure:
     *
     * - id: string|integer, required.
     * - title: string, required, the title of the item.
     * - depth: integer, required, nesting level.
     */
    public $items = [];
    /**
     * @var array
     * - selectParent: boolean, the choice of the parent item.
     * - separator: string, the path separator.
     * - padding: integer, the indent child
     * - showPath: boolean, the way to the title.
     */
    public $nestedOptions = [];
    /**
     * @var array the options for the underlying JS plugin.
     */
    public $clientOptions = [];
    /**
     * @var array
     */
    protected $_clientOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        Html::addCssClass($this->options, ['widget' => 'uk-form-width-large']);

        $js = [];
        $id = $this->options['id'];
        $view = $this->getView();
        $item = $this->items ? current($this->items) : [];
        $this->initPlaceholder();

        if (ArrayHelper::isAssociative($item)) {
            if (array_key_exists('depth', $item)) {
                $this->initNestedOptions();
            } else {
                $this->_clientOptions['data'] = $this->items;
                $this->items = [];
            }
            if ($this->values) {
                $js[] = "jQuery('#$id').val($this->values).trigger('change');";
            }
        }

        // $this->_clientOptions['width'] = '500px';
        $clientOptions = Json::encode(array_replace_recursive($this->_clientOptions, $this->clientOptions));
        $js[] = "jQuery('#$id').select2($clientOptions);";

        $view->registerJs(implode("\n", array_reverse($js)));
        Select2Asset::register($view);
        ThemeDefaultAsset::register($view);
    }

    /**
     * @inheritdoc
     */
    public function run()
	{
        if ($this->hasModel()) {
            return Html::activeDropDownList($this->model, $this->attribute, $this->items, $this->options);
        } else {
            return Html::dropDownList($this->name, $this->value, $this->items, $this->options);
        }
	}

    /**
     * Init nested options
     */
    public function initNestedOptions()
    {
        $defaults = [
            'selectParent' => false,
            'separator' => '/',
            'padding' => 20,
            'showPath' => true,
        ];
        $this->nestedOptions = array_replace_recursive($defaults, $this->nestedOptions);
        $this->_clientOptions['data'] = $this->buildNested();
        $padding = $this->nestedOptions['padding'];
        $this->_clientOptions['templateSelection'] = new JsExpression("function templateSelection(i) { return i.path ? i.path : i.text; }");
        $this->_clientOptions['templateResult'] = new JsExpression("function formatResult(i) { var p=(i.depth>1 ? $padding*(i.depth-1) : 0); if(p){var s='style=\"padding-left:'+p+'px\"'; return $('<span '+s+'>'+i.text+'</span>');} return i.text; }");
    }

    /**
     * Get value
     * @return string the result
     */
    public function getValues()
    {
        if ($this->hasModel()) {
            $value = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            $value = $this->value;
        }
        return Json::encode($value);
    }

    /**
     * Build nested
     * @return array the result
     */
    public function buildNested()
    {
        $result = [];
        $title = [];
        $items = $this->items;
        foreach ($items as $key => $item) {
            $next = next($items);
            $result[] = [
                'id' => $item['id'],
                'text' => $item['title'],
                'depth' => $item['depth'],
            ];

            if ($this->nestedOptions['showPath'] && !empty($title)) {
                $result[$key]['path'] = implode($this->nestedOptions['separator'], $title)
                    . $this->nestedOptions['separator'] . $item['title'];
            }

            if ($item['depth'] > 0 && $next['depth'] > $item['depth']) {
                if (!$this->nestedOptions['selectParent']) {
                    unset($result[$key]['id']);
                    $result[$key]['children'] = true;
                }
                $title[] = $item['title'];
            }

            if ($next['depth'] < $item['depth']) {
                $title = [];
            }
        }
        $this->items = [];
        return $result;
    }

    protected function initPlaceholder()
    {
        $isMultiple = ArrayHelper::getValue($this->options, 'multiple', false);
        if (isset($this->options['prompt']) && !isset($this->clientOptions['placeholder'])) {
            $this->clientOptions['placeholder'] = $this->options['prompt'];
            if ($isMultiple) {
                unset($this->options['prompt']);
            }
            return;
        }
        if (isset($this->options['placeholder'])) {
            $this->clientOptions['placeholder'] = $this->options['placeholder'];
            unset($this->options['placeholder']);
        }
        if (isset($this->clientOptions['placeholder']) && is_string($this->clientOptions['placeholder']) && !$isMultiple) {
            $this->options['prompt'] = $this->clientOptions['placeholder'];
        }
    }
}
